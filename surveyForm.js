$("#current-page").val(location.href);
hotjarSuccess = document.getElementById("hotjar-success")
hotjarForm = document.getElementById("hotjar-form-block")
hotjarHeading = document.getElementById("hotjar-heading")
function closeHotjarSurvey(){
  hotjarForm.style.display = "none"
  hotjarSuccess.style.display = "block"
  hotjarHeading.style.display = "none"
}
var datamail;

$(document).ready(function () {
  $("form#survey-form").on("submit", function (event) {
    var query_string = $(this).serialize();
    console.log(query_string);
    var form_data = objectFromWebFlowQuerystring(query_string);
    console.log(form_data);
    datamail = form_data;
    form_data["Current Page"] = window.location.href;
    form_data.form_name = "Survey Form";

    hitWebhook(
      form_data,
      (url = "https://analytics.blackboardradio.com/form_responses")
    );
    event.preventDefault();
    closeHotjarSurvey()
    return false;
  });

  function objectFromWebFlowQuerystring(
    query_string,
    required_keys = [
      "Profession",
      "LeadSource",
      "Why_online_speaking_programme",
      "Age",
    ]
  ) {
    var params = new URLSearchParams(query_string);
    var obj = {};

    for (const key of params.keys()) {
      if (required_keys.includes(key)) {
        obj[key] = params.get(key);
      }
    }

    obj["LeadSource"] = "Website";
    return obj;
  }

  function hitWebhook(form_data, url) {
    $.ajax({
      url: url,
      data: form_data,
      type: "POST",
      dataType: "json",
      error: function () {
        console.log("error");
      },
      success: function (data) {
        console.log("success");
      },
    });
  }
});
