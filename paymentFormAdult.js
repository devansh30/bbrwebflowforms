$("#current-page").val(location.href);
var datamail;

$(document).ready(function () {
  $("form#wf-form-Payment-Form-Adult").on("submit", function (event) {
    var query_string = $(this).serialize();
    $("#form-submit-btn").attr("disabled", true);
    $("#form-submit-btn").css("background-color", "grey");
    console.log(query_string);
    var form_data = objectFromWebFlowQuerystring(query_string);
    console.log(form_data);
    datamail = form_data;
    form_data["Current Page"] = window.location.href;
    form_data.form_name = "Payment Form Adult";
    hitWebhook(
      form_data,
      (url = "https://analytics.blackboardradio.com/webflow_webhook")
    );
    event.preventDefault();
    setTimeout(function () {
      redirectToPaymentPage();
    }, 2000);
    return false;
  });

  function redirectToPaymentPage() {
    var params = window.location.search;
    var emailParam = "&billing_email=" + datamail["email"];
    var url;
    if (params) {
      url = datamail["email"]
        ? "https://pay.blackboardradio.com/cartflows_step/checkout-page" +
          params +
          "&billing_phone=" +
          datamail["Phone"] +
          emailParam
        : "https://pay.blackboardradio.com/cartflows_step/checkout-page" +
          params +
          "&billing_phone=" +
          datamail["Phone"];
    } else {
      url = datamail["email"]
        ? "https://pay.blackboardradio.com/cartflows_step/checkout-page" +
          "?billing_phone=" +
          datamail["Phone"] +
          emailParam
        : "https://pay.blackboardradio.com/cartflows_step/checkout-page" +
          "?billing_phone=" +
          datamail["Phone"];
    }
    window.location.href = url;
  }
  function objectFromWebFlowQuerystring(
    query_string,
    required_keys = [
      "Child-s-Name",
      "Phone",
      "City",
      "Country-Code",
      "Current Page",
      "LeadSource",
      "parental-expectation",
      "Language-at-Home",
      "email",
    ]
  ) {
    var params = new URLSearchParams(query_string);
    var obj = {};

    for (const key of params.keys()) {
      if (required_keys.includes(key)) {
        obj[key] = params.get(key);
      }
    }

    if ($("#Child-s-Name").val()) {
      obj["Child's Name"] = $("#Child-s-Name").val();
    }

    if ($("input#WhatsappConsent:checked").length >= 1) {
      obj["WhatsappConsent"] = "true";
    } else {
      obj["WhatsappConsent"] = "false";
    }

    let codeNum;
    if ($("#Country-Code").val() == "India") {
      codeNum = "+91";
    } else if ($("#Country-Code").val() == "United Arab Emirates") {
      codeNum = "+971";
    } else if ($("#Country-Code").val() == "Saudi Arabia") {
      codeNum = "+966";
    } else if ($("#Country-Code").val() == "Turkey") {
      codeNum = "+90";
    } else if ($("#Country-Code").val() == "Indonesia") {
      codeNum = "+62";
    } else if ($("#Country-Code").val() == "Philippines") {
      codeNum = "+63";
    } else if ($("#Country-Code").val() == "Brazil") {
      codeNum = "+55";
    } else if ($("#Country-Code").val() == "U.S") {
      codeNum = "+1";
    }

    obj["Country-Code"] = { country: $("#Country-Code").val(), code: codeNum };

    obj["LeadSource"] = "Website";
    return obj;
  }

  function hitWebhook(form_data, url) {
    $.ajax({
      url: url,
      data: form_data,
      type: "POST",
      dataType: "json",
      error: function () {
        console.log("error");
      },
      success: function (data) {
        console.log("success");
      },
    });
  }
});
