function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(
    /[?&]+([^=&]+)=([^&]*)/gi,
    function (m, key, value) {
      vars[key] = value;
    }
  );
  return vars;
}

var currentAid = getUrlVars().adgroupid;

//use class blue-span for blue heading
//use <b> tag to bold text in header and subheader
var rules = [
  {
    aid: "1234",
    header:
      "Best Spoken English Classes for your child. <span class = 'blue-span'>This will be the text in blue</span>",
    subheader:
      "Book a demo session and try our classes. <b>This will be the text in bold</b> And here again we have normal text",
  },
  {
    aid: "5678",
    header: "Grab an opportunity of a lifetime. <span class = 'blue-span'>This will be the text in blue</span>",
    subheader: "You are not gonna miss this.<b>This will be the text in bold</b> And here again we have normal text",
  },
  {
    aid: "9999",
    header: "This is a test Header. You can try. <span class = 'blue-span'>This will be the text in blue</span>",
    subheader: "And so is the subheading. <b>This will be the text in bold</b> And here again we have normal text",
  },
  {
    aid: "0000",
    header: "Time is going up Quickly. <span class = 'blue-span'>This will be the text in blue</span>",
    subheader: "Do not stay behind the crowd. Take decisions ASAP. <b>This will be the text in bold</b> And here again we have normal text",
  },
];

if (currentAid != undefined) {
  rules.forEach((rule) => {
    if (rule.aid == currentAid) {
      document.getElementById("page-header").innerHTML = rule.header;
      document.getElementById("page-sub-header").innerHTML = rule.subheader;
    }
  });
}
