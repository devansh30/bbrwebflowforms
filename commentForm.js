const replyModal = document.getElementById("reply-modal");

const showReplyModal = (name, commentId) => {
  const replyee_name = document.getElementById("replyee_name");
  const replyCommentID = document.getElementById("replyCommentID");

  replyCommentID.value = commentId;
  replyee_name.innerText = name;
  replyModal.style.display = "flex";
  replyModal.style.opacity = "1";
};

const closeReplyModal = () => {
  replyModal.style.display = "none";
  replyModal.style.opacity = "0";
  document.getElementById("replierCommentInput").value = "";
};

$(document).ready(function () {
  // Store the comment in FaunaDB
  const client = new faunadb.Client({
    secret: "fnAFIA6hE_AAUZT99C0Wvr2mw1YObq4k8-OnLYO1",
    // NOTE: Use the correct endpoint for your database's Region Group.
    endpoint: "https://db.fauna.com/",
  });

  const slug = window.location.pathname; // Retrieve the current page slug

  $("form#wf-form-Comment-Form").on("submit", function (event) {
    event.preventDefault();

    const name = $("#nameInput").val();
    const comment = $("#commentInput").val();
    const commentFormBlock = document.getElementById("commentFormBlock");

    const newComment = {
      name,
      comment,
      slug,
      created_at: new Date().toLocaleString("en-IN", {
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        hour12: true,
      }),
    };

    client
      .query(
        faunadb.query.Create(faunadb.query.Collection("webflow_comments"), {
          data: newComment,
        })
      )
      .then((response) => {
        console.log("Comment created successfully:", response);
        fetchComments(slug);
        commentFormBlock.innerHTML = "Thanks for sharing your opinion!!!";
      })
      .catch((error) => {
        console.error("Error creating comment:", error);
      });

    return false;
  });

  $("form#wf-form-Reply-Form").on("submit", function (event) {
    event.preventDefault();

    const name = $("#replierNameInput").val();
    const comment = $("#replierCommentInput").val();
    const replyCommentID = $("#replyCommentID").val();

    const reply = {
      name,
      comment,
      created_at: new Date().toLocaleString("en-IN", {
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        hour12: true,
      }),
    };

    const updateCommentWithReply = async (commentId, reply) => {
      try {
        const commentRef = faunadb.query.Ref(
          faunadb.query.Collection("webflow_comments"),
          commentId
        );
        const comment = await client.query(faunadb.query.Get(commentRef));

        const updatedComment = await client.query(
          faunadb.query.Update(commentRef, {
            data: {
              replies: comment.data.replies
                ? [...comment.data.replies, reply]
                : [reply],
            },
          })
        );

        console.log("Comment updated with reply:", updatedComment);
        closeReplyModal();
        fetchComments(slug);
      } catch (error) {
        console.error("Error updating comment with reply:", error);
      }
    };

    updateCommentWithReply(replyCommentID, reply);

    return false;
  });

  const fetchComments = async (slug) => {
    try {
      const response = await client.query(
        faunadb.query.Map(
          faunadb.query.Paginate(
            faunadb.query.Match(faunadb.query.Index("comments_by_slug"), slug)
          ),
          faunadb.query.Lambda(
            "comment",
            faunadb.query.Merge(
              {
                id: faunadb.query.Select(
                  ["ref", "id"],
                  faunadb.query.Get(faunadb.query.Var("comment"))
                ),
              },
              faunadb.query.Select(
                ["data"],
                faunadb.query.Get(faunadb.query.Var("comment"))
              )
            )
          )
        )
      );

      const comments = response.data.map((comment) => comment);

      const publishedCommentsContainer = document.getElementById("published_comments");
      publishedCommentsContainer.innerHTML = "";

      if (comments.length === 0) {
        publishedCommentsContainer.innerHTML = "Be the first one to comment.";
      } else {
        comments.forEach((comment) => {
          const commentElement = document.createElement("div");
          commentElement.classList.add("each_comment");
          commentElement.innerHTML = `<div>${comment.name}<span class="comment_time">${comment.created_at}</span></div><div class="comment_text">${comment.comment}</div><button class="reply_btn" onclick="showReplyModal('${comment.name}','${comment.id}')">⤴️ Reply</button>`;

          if (comment.replies && comment.replies.length > 0) {
            const repliesContainer = document.createElement("div");
            repliesContainer.classList.add("replies_container");

            comment.replies.forEach((reply) => {
              const replyElement = document.createElement("div");
              replyElement.classList.add("comment_reply");
              replyElement.innerHTML = `<div>${reply.name}<span class="comment_time">${reply.created_at}</span></div><div class="comment_text">${reply.comment}</div>`;

              repliesContainer.appendChild(replyElement);
            });

            commentElement.appendChild(repliesContainer);
          }

          publishedCommentsContainer.insertBefore(commentElement, publishedCommentsContainer.firstChild);
        });
      }
    } catch (error) {
      console.error("Error fetching comments:", error);
    }
  };

  fetchComments(slug);
});
