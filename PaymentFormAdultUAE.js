$("#current-page").val(location.href);
var datamail;

$(document).ready(function () {
  $("form#wf-form-Payment-Form-Adult-UAE").on("submit", function (event) {
    var query_string = $(this).serialize();
    $("#form-submit-btn").attr("disabled", true);
    $("#form-submit-btn").css("background-color", "grey");
    console.log(query_string);
    var form_data = objectFromWebFlowQuerystring(query_string);
    console.log(form_data);
    datamail = form_data;
    form_data["Current Page"] = window.location.href;
    form_data.form_name = "Payment Form Adult UAE";
    hitWebhook(
      form_data,
      (url = "https://analytics.blackboardradio.com/webhook")
    );
    hitWebhook(
      form_data,
      (url = "https://analytics.blackboardradio.com/webflow_webhook")
    );
    hitWebhook(
      form_data,
      (url = "https://webhook.site/19aa7ccd-4320-4b35-8235-3109258562c6")
    );
    event.preventDefault();
    setTimeout(function () {
      redirectToPaymentPage();
    }, 2000);
    return false;
  });

  function redirectToPaymentPage() {
    var params = window.location.search;
    var emailParam = "&email=" + datamail["email"];
    var url;
    if (params) {
      url = datamail["email"]
        ? "https://pages.razorpay.com/pl_Jf3tpfUknA5ASk/view" +
          params +
          "&phone=" +
          datamail["Phone"] +
          emailParam + "&name=" + datamail["Child's Name"]
        : "https://pages.razorpay.com/pl_Jf3tpfUknA5ASk/view" +
          params +
          "&phone=" +
          datamail["Phone"] + "&name=" + datamail["Child's Name"];
    } else {
      url = datamail["email"]
        ? "https://pages.razorpay.com/pl_Jf3tpfUknA5ASk/view" +
          "?phone=" +
          datamail["Phone"] +
          emailParam + "&name=" + datamail["Child's Name"]
        : "https://pages.razorpay.com/pl_Jf3tpfUknA5ASk/view" +
          "?phone=" +
          datamail["Phone"] + "&name=" + datamail["Child's Name"];
    }
    window.location.href = url;
  }
  function objectFromWebFlowQuerystring(
    query_string,
    required_keys = [
      "Child-s-Name",
      "Phone",
      "City",
      "courseChosen",
      "Current Page",
      "LeadSource",
      "parental-expectation",
      "Language-at-Home",
      "email",
      "CountryCode"
    ]
  ) {
    var params = new URLSearchParams(query_string);
    var obj = {};

    for (const key of params.keys()) {
      if (required_keys.includes(key)) {
        obj[key] = params.get(key);
      }
    }

    if ($("#Child-s-Name").val()) {
      obj["Child's Name"] = $("#Child-s-Name").val();
    }

    if ($("input#WhatsappConsent:checked").length > 1) {
      obj["WhatsappConsent"] = "true";
    } else {
      obj["WhatsappConsent"] = "false";
    }

    obj["LeadSource"] = "Website";
    return obj;
  }

  function hitWebhook(form_data, url) {
    $.ajax({
      url: url,
      data: form_data,
      type: "POST",
      dataType: "json",
      error: function () {
        console.log("error");
      },
      success: function (data) {
        console.log("success");
      },
    });
  }
});